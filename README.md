# CommonSpace

> A self-hostable platform for hosting groups.

**CommonSpace** is a place for people to build communities. The largest premise
of the platform is that people use it as a _communal_ space. The content and
information is only a copy of what people decide to post using their own website.

## Technologies

* [Micropub][] to allow people to post to their own online presence.
* [Webmention][] to send and receive information around the Web.
* [IndieAuth][] to allow people to sign in using their own online presences.
* [ActivityPub][] to provide interactivity with the [federated Web][1].

## TODO

 * Allow sign-in using [IndieAuth][].
 * CRUD logic for groups.
 * Emit webmentions on C(R)UD operations to group pages and posts.

## Groups Layout

Each group *MUST* have the following:

- Name
- Description
- Visibility Level

Each group *SHOULD* have the following:

- Images
  - Banner (maybe multiple?)
  - Logo (maybe multiple?)
- Website (maybe multiple?)
- Location (multiple ones)
- Onboarding message (ideally one)
- Prejoining questionnaire (definitely a set)

### Visibility

- **Public**: Searchable by general Web, public.
- **Protected**: Not available on Web searches, requires log-in to view
- **Private**: Protected and requires permission from member of the group


[indieauth]: http://indieauth.spec.indieweb.org/
[micropub]: http://micropub.spec.indieweb.org/
[webmention]: http://webmention.spec.indieweb.org/
[indieAuth]: http://indieauth.spec.indieweb.org/
[activityPub]: https://www.w3.org/TR/activitypub/
[1]: http://the-federation.info/